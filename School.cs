﻿using System;

namespace c_classes
{
    class School
    {


        public School()
        {

        }
        static void Main(string[] args)
        {

            Helper help = new Helper();
            help.write("--------------------------------------------------------------------------------------------------------------------------");
            help.write("| This serves as TEST class to demonstrate other classes are working with a defined methods, fields and properties on it.|");
            help.write("--------------------------------------------------------------------------------------------------------------------------");
            help.write("School class is working...");

            Teachers teacher = new Teachers();
            Classes _class = new Classes();

            //How to use classes... explained here
            help.br();
            help.write("------------------- Working on Student Class Example--------------------");
            Students student1 = new Students();
            student1.setName("Mel Adlrin L. Reyes");
            student1.setStudentID(10011);

            Students student2 = new Students();
            student2.setName("Mark Anthony L. Reyes");
            student2.setStudentID(10013);

            //Outputting Student Details
            student1.getStudentData();
            help.br(); //adding some space/new line here ... refer to helper.cs for details
            help.write("Student 2 Details");
            student2.getStudentData();
            help.write("-------------------- ENDS HERE ----------------------------------------");
            //Ends here

        }
    }
}
