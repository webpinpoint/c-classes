using System;

namespace c_classes
{
    class Students : Helper
    {
        private string studentName;
        private int studentID;
        public Students()
        {
            Helper help = new Helper();
            help.write("Student class is working...");
        }


        public string setName(string name)
        {
            return this.studentName = name;
        }

        public string getName()
        {
            return this.studentName;
        }

        public int setStudentID(int id)
        {
            return this.studentID = id;
        }
        public int getStudentID()
        {
            return this.studentID;
        }

        public void getStudentData()
        {
            Helper help = new Helper();
            help.write("Student Name: " + this.studentName);
            help.write("Student ID: " + this.studentID);
        }



    }
}