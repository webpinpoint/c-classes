using System;

namespace c_classes
{
    class Helper
    {
        public Helper()
        {
            // this.write("Initializing helper...");
        }

        public void write(string text)
        {
            Console.WriteLine(text);
        }

        public void br()
        {
            Console.WriteLine("");
        }
    }
}