using System;

namespace c_classes
{
    class Classes
    {
        private string classCode;
        private int noOfTeachers;
        public Classes()
        {
            Helper help = new Helper();
            help.write("Classes class is working...");
        }

        public string setCode(string code)
        {
            return this.classCode = code;
        }

        public string getCode()
        {
            return this.classCode;
        }

        public int setNoOfTeachers(int teachers)
        {
            return this.noOfTeachers = teachers;
        }

        public int getNotOfTeachers()
        {
            return this.noOfTeachers;
        }

    }
}